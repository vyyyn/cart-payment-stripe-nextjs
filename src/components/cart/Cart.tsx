"use client";
import { Box, Button, Grid, IconButton, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { ArrowBackIos } from "@mui/icons-material";
import { loadStripe } from "@stripe/stripe-js";
import {
  PaymentElement,
  Elements,
  useStripe,
  useElements,
  CartElement,
} from "@stripe/react-stripe-js";
import PaymentForm from "./PaymentForm";
import Purchase from "./Purchase";

export default function Cart() {
  const [clientSecret, setClientSecret] = useState<string>();

  const stripePromise = loadStripe(
    process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY as string
  );
  const options = {
    clientSecret,
  };

  return (
    <>
      {!clientSecret ? (
        <PaymentForm setClientSecret={setClientSecret} />
      ) : (
        <Box sx={{ width: "600px", m: "0 auto" }}>
          <Elements stripe={stripePromise} options={options}>
            <Purchase />
          </Elements>
        </Box>
      )}
    </>
  );
}

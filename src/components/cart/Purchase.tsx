import React, { FormEvent, useState } from "react";
import {
  CardElement,
  CartElement,
  PaymentElement,
  useCartElement,
  useElements,
  useStripe,
} from "@stripe/react-stripe-js";
import { Button } from "@mui/material";
export default function Purchase() {
  const domain = window.location.origin;
  const [process, setProcess] = useState<boolean>(false);
  const [mess, setMess] = useState<string>();
  const elements = useElements();
  const stripe = useStripe();
  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!stripe || !elements) {
      return;
    }
    const cardElement = elements?.getElement(CardElement);
    setProcess(true);
    const { error } = await stripe.confirmPayment({
      elements,
      confirmParams: {
        // Make sure to change this to your payment completion page
        return_url: domain,
      },
    });
    if (error.type === "card_error" || error.type === "validation_error") {
      setMess(error.message);
    } else {
      setMess("An unexpected error occurred.");
    }
    setProcess(false);
  };
  return (
    <form id="payment-form" onSubmit={handleSubmit}>
      <PaymentElement />
      <Button type="submit" variant="contained" fullWidth color="primary">
        Pay now
      </Button>
    </form>
  );
}

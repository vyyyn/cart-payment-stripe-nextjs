import { ArrowBackIos } from "@mui/icons-material";
import { Box, Button, Grid, IconButton, Typography } from "@mui/material";

import React, { useEffect, useState } from "react";
const cart = {
  total: 2,
  data: [
    {
      id: 1,
      name: "Croissant",
      price: 200,
      quantity: 1,
    },
    {
      id: 2,
      name: "Cookie",
      price: 100,
      quantity: 5,
    },
  ],
};
interface Props {
  setClientSecret: (val: string) => void;
}
export default function PaymentForm({ setClientSecret }: Props) {
  const [total, setTotal] = useState<number>();
  useEffect(() => {
    if (cart) {
      setTotal(
        cart.data.reduce((total, item) => {
          return total + item.price * item.quantity;
        }, 0)
      );
    }
  }, []);
  const handlePayment = async () => {
    const url = "http://localhost:8080/stripe";
    await fetch(url, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ item: cart.data }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        setClientSecret(data.client_secret);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };
  return (
    <Grid
      container
      item
      md={12}
      spacing={2}
      sx={{
        background: "white",
        borderRadius: "20px",
        p: "24px",
        m: "36px 0 0",
      }}
    >
      {/* CART LIST */}
      <Grid md={8} item>
        <Box
          sx={{ display: "flex", justifyContent: "space-between", mb: "24px" }}
        >
          <Box display="flex" alignItems="center">
            <IconButton>
              <ArrowBackIos />
            </IconButton>
            <Typography color="primary" fontWeight="bold" variant="h6">
              Cart
            </Typography>
          </Box>

          <Typography
            sx={{
              background: "#2f3645",
              color: "white",
              borderRadius: "20px",
              fontSize: "12px",
              display: "flex",
              alignItems: "center",
              p: "0 24px",
            }}
          >
            Total items: {cart?.total}
          </Typography>
        </Box>

        {cart.data.map((v) => {
          return (
            <Box
              key={v?.id}
              sx={{
                p: "24px",
                borderRadius: "20px",
                display: "flex",
                justifyContent: "space-between",
                borderBottom: "1px solid #dfdfdf",
                mb: "12px",
                pb: "24px",
                background: "white",
              }}
            >
              <Box>
                <Typography>{v?.name}</Typography>
                <Typography fontSize="12px">
                  Quantity: {v?.quantity}{" "}
                </Typography>
              </Box>
              <Box>
                <Typography fontWeight="bold">${v?.price ?? 0}</Typography>
              </Box>
            </Box>
          );
        })}
      </Grid>
      {/* PAYMENT */}
      <Grid md={4} item sx={{ height: "80vh" }}>
        <Box
          sx={{
            background: "#2f3645",
            p: "24px",
            color: "white",
            borderRadius: "20px",
            height: "100%",
          }}
        >
          <Box mb="24px">
            <Typography
              variant="h6"
              color="white"
              mb="12px"
              borderBottom="1px solid #888888"
              pb="8px"
              fontWeight="bold"
            >
              Shipping address
            </Typography>
            <Typography fontSize="14px">Chloe Nguyen</Typography>
            <Typography fontSize="14px">+84 xxx xxx</Typography>
            <Typography fontSize="14px">Vietnam</Typography>
          </Box>
          <Box>
            <Typography
              variant="h6"
              color="white"
              mb="12px"
              borderBottom="1px solid #888888"
              pb="8px"
              fontWeight="bold"
            >
              Order sumary
            </Typography>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography mb="12px" fontSize="14px">
                Subtotal:
              </Typography>
              <Typography>{"$" + total + ".00"}</Typography>
            </Box>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography mb="12px" fontSize="14px">
                Shipping
              </Typography>
              <Typography mb="12px" fontSize="14px">
                $15
              </Typography>
            </Box>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography mb="12px" fontSize="18px">
                Total
              </Typography>
              <Typography mb="12px" fontSize="18px">
                ${(total as number) + 15 + ".00" ?? 0}
              </Typography>
            </Box>
          </Box>
          <Button
            onClick={handlePayment}
            sx={{ mt: "100%" }}
            variant="contained"
            fullWidth
          >
            Pay now
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
}
